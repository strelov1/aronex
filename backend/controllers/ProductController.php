<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\Product;
use backend\models\ProductSearch;
use common\models\Category;
use common\models\Label;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Image;
use backend\models\MultipleUploadForm;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','view','create','update','delete','delete-image'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $categories = Category::find()->all();
        $label = Label::find()->all();
        $form = new MultipleUploadForm();

        if ( $model->load(Yii::$app->request->post()) ) {
            $model->save();
            $form->files = UploadedFile::getInstances($form, 'files');
            if ($form->files != NULL) {
                if ($form->files && $form->validate()) {
                    foreach ($form->files as $file) {
                        $image = new Image();
                        $image->product_id =  $model->id;
                        $image->url = $model->id . '-' . $file->name;
                        if ($image->save()) {
                            $file->saveAs($image->getPath($model->id . '-' . $file->name));
                        }
                    }

                }
            }
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'categories' => $categories,
                'label' => $label,
                'uploadForm' => $form,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $categories = Category::find()->all();
        $label = Label::find()->all();
        $form = new MultipleUploadForm();

        if ( $model->load(Yii::$app->request->post()) ) {
            $form->files = UploadedFile::getInstances($form, 'files');
            if ($form->files != NULL) {
                if ($form->files && $form->validate()) {
                    foreach ($form->files as $file) {
                        $image = new Image();
                        $image->product_id = $id;
                        $image->url = $id . '-' . $file->name;
                        if ($image->save()) {
                            $file->saveAs($image->getPath($id . '-' . $file->name));
                        }
                    }

                }
            }
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'categories' => $categories,
                'label' => $label,
                'uploadForm' => $form,
            ]);
        }
    }

    public function actionDeleteImage($id)
    {
        $image = Image::find()->where(['id' => $id])->one();
        $image->delete();
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
