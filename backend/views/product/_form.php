<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use vova07\imperavi\Widget as Imperavi;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map($categories, 'id', 'title'), ['prompt' => 'Выберите категорию']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(Imperavi::className(), [
        'settings' => [
            'lang' => Yii::$app->language,
            'minHeight' => 200,
            'plugins' => [
                'fullscreen',
                ],
            ],
    ]); ?>


    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'label_id')->dropDownList(ArrayHelper::map($label, 'id', 'title'), ['prompt' => 'Выберите метку']) ?>

    <?= $form->field($uploadForm, 'files[]')->fileInput(['multiple' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php
    if(!empty($model->images)){
        $dir = Yii::getAlias('http://aronex.ru/images/product/');
        foreach ($model->images as $image){
            echo Html::img(Url::to($dir.$image->url),[
                'width' => '150;'
            ]);
            echo '<br>';
            echo Html::a('Удалить',['delete-image', 'id' => $image->id]);
            echo '<br>';
        }
    }
    ?>

</div>
