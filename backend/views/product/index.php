<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <p>
        <?= Html::a('Добавить товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//             'id',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($model) {
                    $dir = Yii::getAlias('http://aronex.ru/images/product/');
                    return empty($model->images) ? '-' : Html::img(Url::to($dir.$model->images[0]->url),[
                        'width' => '140;'
                    ]);
                },
            ],
             'title',
            [
                'attribute'=>'catalog_id',
                'value' => function ($model) {
                    return empty($model->category_id) ? '-' : $model->category->title;
                },
                'filter' => Html::activeDropDownList(
                        $searchModel,
                        'category_id',
                        Product::getArrayCategory(),
                        ['class' => 'form-control', 'prompt' => '']
                    )
            ],
//             'description',
             'price',
             'label_id',
             'slug',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
