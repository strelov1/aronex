<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Admin</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Магазин', 'options' => ['class' => 'header']],
                    ['label' => 'Категории', 'url' => ['/category/index']],
                    ['label' => 'Товары', 'url' => ['/product/index']],
                    ['label' => 'Заказы', 'url' => ['/order/index']],
                    ['label' => 'Обратный звонок', 'url' => ['/order-call']],
                    ['label' => 'Настройки', 'options' => ['class' => 'header']],
                    ['label' => 'Страницы', 'url' => ['/pages/manager']],
                    ['label' => 'Новости', 'url' => ['/news/admin']],
                    ['label' => 'Слайдер', 'url' => ['/banner']],
                ],
            ]
        ) ?>

    </section>

</aside>
