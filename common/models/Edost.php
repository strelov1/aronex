<?php

namespace common\models;

use linslin\yii2\curl;

class Edost
{
    const EDOST_ID = 7114;
    const EDOST_PSWD = 'BgHlfHgTLyErhIMlaq5DVOmWCiZIQTtA';
    const EDOST_URL = 'http://www.edost.ru/edost_calc_kln.php';

    public static function calc($city)
    {
        $city = trim($city);
        if(!strpos($city, 'г ')){
            $city = end(explode('г ',$city));
        }

        $curl = new curl\Curl();
        $curl->setOption(
            CURLOPT_POSTFIELDS,
            http_build_query([
                    'id' => self::EDOST_ID,
                    'p' => self::EDOST_PSWD,
                    'to_city' => $city,
                    'weight' => 2,
                    'strah' => 100,
                ]
            ))
            ->post(self::EDOST_URL);

        $xml = simplexml_load_string($curl->response);

        if(isset($xml->tarif[0])){
            $delevery = [];
            $i = 1;
            foreach ($xml->tarif as $tarif){
                switch ($tarif->company) {
                    case "Почта России":
                        $img = '<img src="/images/russian_post.gif">';
                        break;

                    case "EMS Почта России":
                        $img = '<img src="/images/ems.gif">';
                        break;

                    case "СДЭК":
                        $img = '<img src="/images/sdek.gif">';
                        break;

                    case "Курьер":
                        $img = '<img src="/images/default.gif">';
                        break;

                    default:
                        $img = '<img src="/images/default.gif">';
                        break;
                }
                $delevery[$i]['id']      = $i;
                $delevery[$i]['name']    = $img.'<br>'.$tarif->company.' '.$tarif->name.' '.$tarif->price.' руб.';
                $delevery[$i]['tarif']   = $tarif->company.' '.$tarif->name;
                $delevery[$i]['price']   = (float)$tarif->price;
                $i++;
            }
            return $delevery;
        } else {
            return false;
        }
    }

    public static function calcOne($id,$city)
    {
        return self::calc($city)[$id];
    }
}