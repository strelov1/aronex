<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $phone
 * @property string $address
 * @property string $email
 * @property string $notes
 * @property string $delevery
 * @property string $payment
 * @property string $status
 *
 * @property OrderItem[] $orderItems
 */
class Order extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 'Новый заказ';
    const STATUS_IN_PROGRESS = 'Подвержден';
    const STATUS_PAYMENT = 'Оплачен';
    const STATUS_DONE = 'Выполнен';

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','surname','patronymic','phone','email','city','street','house','delevery','delevery_cost','payment'], 'required'],
            [['notes'], 'string'],
            [['postal_code','inn','ogrn','current_account','bik','kor_account'], 'integer'],
            [['name','surname','patronymic','phone','email','region','city','street','house','apartment','delevery','payment','orgname','bank'], 'string', 'max' => 255],
            [['email'], 'email'],
        ];
    }


    public function scenarios()
	{
	    $scenarios = parent::scenarios();
        $scenarios['default'] = ['name','surname','patronymic','phone','email','region','city','street','house','apartment','postal_code','notes'];
	    $scenarios['one-ur'] = ['name','surname','patronymic','phone','email','region','city','street','house','apartment','postal_code','notes','orgname','inn','ogrn','current_account','bik','kor_account','bank'];
	    $scenarios['delevery'] = ['delevery','delevery_cost'];
	    $scenarios['payment'] = ['payment'];

	    return $scenarios;
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',

            'name' => 'Имя',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'phone' => 'Номер телефона',
            'email' => 'Email',

            'region' => 'Регион',
            'city' => 'Город',
            'street' => 'Улица',
            'house' => 'Дом',
            'apartment' => 'Квартира',
            'postal_code' => 'Индекс',

            'delevery' => 'Способ доставки',
            'delevery_cost' => 'Стоимость доставки',
            'payment' => 'Способ оплаты',
            'notes' => 'Комментарии',
            'status' => 'Статус',

            'orgname' => 'Наименование организации',
            'inn' => 'ИНН',
            'ogrn' => 'ОГРН',
            'current_account' => 'Расчетный счет',
            'bik' => 'БИК',
            'kor_account' => 'Кор. счет',
            'bank' => 'Банк',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }


    public static function getDelevery($city)
    {
        $edost = Edost::calc($city);
        return $edost ? ArrayHelper::map($edost, 'id', 'name') : false;
    }


    public static function getDeleveryOne($id, $city)
    {
        return Edost::calcOne($id,$city);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->status = self::STATUS_NEW;
            }
            return true;
        } else {
            return false;
        }
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_DONE => 'Выполнен',
            self::STATUS_IN_PROGRESS => 'Подвержден',
            self::STATUS_NEW => 'Новый заказ',
        ];
    }

    public function sendEmail()
    {
        return Yii::$app->mailer->compose('order', ['order' => $this])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setSubject('Новый заказ #' . $this->id)
            ->send();
    }

    public function afterSave($insert, $changedAttributes){
        Yii::$app->session->set('order', $this->id);
    }
}
