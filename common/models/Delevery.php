<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "delevery".
 *
 * @property integer $id
 * @property string $name
 * @property double $fix_cost
 * @property integer $input
 */
class Delevery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'delevery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fix_cost'], 'number'],
            [['input'], 'integer'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'fix_cost' => 'Fix Cost',
            'input' => 'Input',
        ];
    }
}
