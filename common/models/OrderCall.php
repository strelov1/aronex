<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_call".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 */
class OrderCall extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_call';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'phone'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
        ];
    }

    public function sendEmail($email)
    {
        $body = 'Заказан обратный звонок<br>Имя: <b>'.$this->name.'</b><br>Телефон: <b>'.$this->phone.'</b>';
        return Yii::$app->mailer->compose()
            ->setFrom($email)
            ->setTo($email)
            ->setSubject('Заказ обратного звонока - Aronex.ru')
            ->setHtmlBody($body)
            ->send();
    }
}
