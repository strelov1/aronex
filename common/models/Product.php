<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;
use yii\helpers\ArrayHelper;
use common\models\Category;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $id_parser
 * @property integer $category_id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property double $price
 * @property integer $label_id
 *
 * @property Attribute[] $attributes
 * @property Image[] $images
 * @property OrderItem[] $orderItems
 * @property Category $category
 * @property Label $label
 */
class Product extends \yii\db\ActiveRecord
{
    use CartPositionTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'label_id'], 'integer'],
            [['category_id', 'title'], 'required'],
            ['title', 'required'],
            [['description'], 'string'],
            [['price'], 'number'],
            [['title', 'slug'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'title' => 'Наименование',
            'slug' => 'URL',
            'description' => 'Описание',
            'price' => 'Цена',
            'label_id' => 'Метка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributes($names = null, $except = [])
    {
        return $this->hasMany(Attribute::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLabel()
    {
        return $this->hasOne(Label::className(), ['id' => 'label_id']);
    }

    /**
     * @inheritdoc
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    public static function getArrayCategory()
    {
        return ArrayHelper::map(Category::find()->all(), 'id', 'title');
    }
}
