<?php
return [
    'adminEmail' => 'info@aronex.ru',
    'supportEmail' => 'info@aronex.ru',
    'user.passwordResetTokenExpire' => 3600,
];
