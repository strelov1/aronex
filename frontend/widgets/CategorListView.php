<?php

namespace frontend\widgets;

use Yii;

class CategorListView extends \yii\widgets\ListView
{
    public $separatorListCount = 0;
    public $separatorListOne = "\n";
    public $separatorListDiv = null;

    /**
     * Renders all data models.
     * @return string the rendering result
     */
    public function renderItems()
    {
        $models = array_values($this->dataProvider->getModels());
        $keys = $this->dataProvider->getKeys();
        $rows = [];

        $modelsLength = count($models);
        $i = 1;
        foreach ($models as $index => $model) {
            if($this->separatorListDiv !== null){
                /**
                 * Добавляет открывающийся тег $separatorListDiv при первой итерации цикла
                 *
                 */
                if($i == 1){
                    $rows[] = '<div class="'.$this->separatorListDiv.'">';
                }
                /**
                 * Добавляет Сгенерированный элемент в массив
                 *
                 */
                $rows[] = $this->renderItem($model, $keys[$index], $index);
                /**
                 * Добавляет $separatorListOne каждый цикл
                 *
                 */
                if($i%$this->separatorListCount == 0){
                    $rows[] = $this->separatorListOne;
                }
                /**
                 * Добавляет закрывающий тег $separatorListDiv при separatorListCount итерации
                 *
                 */
                if($i%$this->separatorListCount == 0){
                    $rows[] = "</div>";
                }
                /**
                 * Добавляет открывающийся тег $separatorListDiv при separatorListCount итерации кроме последней
                 *
                 */
                if( ($i%$this->separatorListCount == 0) and ($i != $modelsLength) ){
                    $rows[] = '<div class="'.$this->separatorListDiv.'">';
                }
                /**
                 * Добавляет закрывающий тег $separatorListDiv при последней если не четный
                 *
                 */
                if(($i == $modelsLength) ){
                    if(($modelsLength & 1) ){
                        $rows[] = "</div>";
                    }
                }
            } else{
                $rows[] = $this->renderItem($model, $keys[$index], $index);
                if($i%$this->separatorListCount == 0){
                    $rows[] = $this->separatorListOne;
                }
            }
            $i++;
        }
        return implode($this->separator, $rows);
    }
}
