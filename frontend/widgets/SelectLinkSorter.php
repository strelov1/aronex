<?php

namespace frontend\widgets;

use Yii;
use yii\helpers\Html;

class SelectLinkSorter extends \yii\widgets\LinkSorter
{
//    protected function renderSortLinks()
//    {
//        $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
//        return Html::renderSelectOptions($this->linkOptions,$attributes);
//    }
    protected function renderSortLinks()
    {
        $attributes = empty($this->attributes) ? array_keys($this->sort->attributes) : $this->attributes;
        $links = [];
        foreach ($attributes as $name) {
            $links[] = $this->sort->link($name, $this->linkOptions);
        }

        return Html::ul($links, array_merge($this->options, ['encode' => false]));
    }
}
