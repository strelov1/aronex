<section class="section_offset">
	<h3 class="offset_title">Рекомендуемые товары</h3>
	<div class="owl_carousel related_products">

		<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->
		<div class="product_item">
			<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->
			<div class="image_wrap">
				<img src="/theme/images/product_img_30.jpg" alt="">
				<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->
				<div class="actions_wrap">
					<div class="centered_buttons">
						<a href="#" class="button_blue add_to_cart">Add to Cart</a>
					</div><!--/ .centered_buttons -->
				</div><!--/ .actions_wrap-->
				<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->
			</div><!--/. image_wrap-->
			<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->
			<!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->
			<div class="label_new">New</div>
			<!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->
			<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->
			<div class="description">
				<a href="#">Leo vel metus nulla facilisi etiam cursus 750mg...</a>
				<div class="clearfix product_info">
					<p class="product_price alignleft"><b>$44.99</b></p>
				</div>
			</div>
			<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->
		</div><!--/ .product_item-->
		
		<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

	</div><!--/ .owl_carousel -->
</section><!--/ .section_offset -->