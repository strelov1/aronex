<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = \yii\helpers\StringHelper::truncateWords($model->title , 7, '...');
// $description = \yii\helpers\StringHelper::truncate($model->description, 150, '...');
// $this->registerMetaTag(['name' => 'description', 'content' => $description]);
if(!empty($model->category->parent_id)){
	$this->params['breadcrumbs'][] = ['label' => $parent_categor->title, 'url' => ['/catalog/list', 'id' => $parent_categor->id]];
}
$this->params['breadcrumbs'][] = ['label' => $model->category->title, 'url' => ['/catalog/list', 'id' => $model->category->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->
<div class="secondary_page_wrapper">
	<div class="container">
		<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
		<?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
		<!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->
		<div class="row">
			<main class="col-md-9 col-sm-8">
				<!-- - - - - - - - - - - - - - Product images & description - - - - - - - - - - - - - - - - -->
				<section class="section_offset">
					<div class="clearfix">
						<!-- - - - - - - - - - - - - - Product image column - - - - - - - - - - - - - - - - -->
						<div class="single_product">
							<!-- - - - - - - - - - - - - - Image preview container - - - - - - - - - - - - - - - - -->
							<div class="image_preview_container">
								<?php if(!empty($model->images)): ?>
									<?= Html::img('/images/product/'.$model->images[0]->url, [
									'id' => 'img_zoom',
									'width' => '320',
									]);?>
								<?php else: ?>
									<img src="/theme/images/qv_img_1.jpg" />
								<?php endif; ?>
								<button class="button_grey_2 icon_btn middle_btn open_qv">
									<i class="icon-resize-full-6"></i>
								</button>
							</div><!--/ .image_preview_container-->

							<!-- - - - - - - - - - - - - - End of image preview container - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Prodcut thumbs carousel - - - - - - - - - - - - - - - - -->

							<?php if(!empty($model->images)): ?>
							<div class="product_preview">
								<div class="owl_carousel" id="thumbnails">
									<?php foreach ($model->images as $image): ?>
									<a href="#" data-image="<?= '/images/product/'.$image->url; ?>" data-zoom-image="<?= '/images/product/'.$image->url; ?>">
										<?= Html::img('/images/product/'.$image->url, [
											'data-large-image' => '/images/product/'.$image->url,
											'width' => '80',
										]);?>
									</a>
									<?php endforeach; ?>
								</div><!--/ .owl-carousel-->
							</div><!--/ .product_preview-->
							<?php endif; ?>

							
							<!-- - - - - - - - - - - - - - End of prodcut thumbs carousel - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Share - - - - - - - - - - - - - - - - -->
							
							<!-- - - - - - - - - - - - - - End of share - - - - - - - - - - - - - - - - -->
						</div>

						<!-- - - - - - - - - - - - - - End of product image column - - - - - - - - - - - - - - - - -->

						<!-- - - - - - - - - - - - - - Product description column - - - - - - - - - - - - - - - - -->

						<div class="single_product_description">
							<h3 class="offset_title"><?= Html::encode($this->title) ?></h3>
							<?php /*
							<div class="description_section v_centered">
								<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->
								<ul class="rating">
									<li class="active"></li>
									<li class="active"></li>
									<li class="active"></li>
									<li></li>
									<li></li>
								</ul>
								<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

								<!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->
								<ul class="topbar">
									<li><a href="#">3 Отзыва</a></li>
									<li><a href="#">Добавить ваш отзыв</a></li>
								</ul>
								<!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->
							
							
							</div>
							<div class="description_section">
								<table class="product_info">
									<tbody>
										<tr>
											<td>Производитель: </td>
											<td><a href="#">Chanel</a></td>
										</tr>
										<tr>
											<td>Наличиее: </td>
											<td><span class="in_stock">в наличии</span> 20 штук</td>
										</tr>
										<tr>
											<td>Артикул: </td>
											<td>PS06</td>
										</tr>
									</tbody>
								</table>
							</div>*/ ?>
							<hr>
							<div class="description_section">
								<p><?= $model->description ?></p>
							</div>
							<hr>
							<p class="product_price"><b class="theme_color"><?= Html::encode($model->price) ?> руб.</b></p>

							<!-- - - - - - - - - - - - - - Quantity - - - - - - - - - - - - - - - - -->

							<div class="description_section_2 v_centered">
								<?= Html::a("Добавить в корзину", ['cart/add', 'id' => $model->id], ['class' => 'button_blue middle_btn']) ?>
							</div>

							<!-- - - - - - - - - - - - - - End of quantity - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

							<div class="buttons_row">
								
							</div>

							<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

						</div>

						<!-- - - - - - - - - - - - - - End of product description column - - - - - - - - - - - - - - - - -->

					</div>
				</section><!--/ .section_offset -->

				<!-- - - - - - - - - - - - - - End of product images & description - - - - - - - - - - - - - - - - -->

				<!-- - - - - - - - - - - - - - Tabs - - - - - - - - - - - - - - - - -->

				<div class="section_offset">
					<div class="tabs type_2">

						<!-- - - - - - - - - - - - - - Navigation of tabs - - - - - - - - - - - - - - - - -->

						<ul class="tabs_nav clearfix">
							<li><a href="#tab-1">Описание</a></li>
							<?php if(!empty($atributes)): ?>
								<li><a href="#tab-2">Характеристики</a></li>
							<?php endif; ?>
							<?php if(!empty($reviews)): ?>
								<li><a href="#tab-3">Отзывы</a></li>
							<?php endif; ?>
						</ul>
					
						<!-- - - - - - - - - - - - - - End navigation of tabs - - - - - - - - - - - - - - - - -->

						<!-- - - - - - - - - - - - - - Tabs container - - - - - - - - - - - - - - - - -->

						<div class="tab_containers_wrap">

							<!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->

							<div id="tab-1" class="tab_container">
								<p><?= $model->description ?></p>
							</div><!--/ #tab-1-->

							<!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
							<?php /*
							<!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->

							<div id="tab-2" class="tab_container">
								<ul class="specifications">
								<?php if(isset($atributes)): ?>
									<?php foreach($atributes as $atribute): ?>
										<li><span><?= $atribute->name; ?></span><?= $atribute->value; ?></li>
									<?php endforeach; ?>
								<?php endif; ?>
								</ul>
							</div><!--/ #tab-2-->

							<!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->

							<!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->

							<div id="tab-3" class="tab_container">

								<?= $this->render('reviews', [
								    'model' => $model,
								]) ?>
								
							</div><!--/ #tab-3-->

							<!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
							*/
							?>
						</div><!--/ .tab_containers_wrap -->

						<!-- - - - - - - - - - - - - - End of tabs containers - - - - - - - - - - - - - - - - -->

					</div><!--/ .tabs-->
				</div><!--/ .section_offset -->

				<!-- - - - - - - - - - - - - - End of tabs - - - - - - - - - - - - - - - - -->


				<!-- - - - - - - - - - - - - - Related products - - - - - - - - - - - - - - - - -->

				<?php /*= $this->render('related');*/  ?>

				<!-- - - - - - - - - - - - - - End of related products - - - - - - - - - - - - - - - - -->
				
			</main><!--/ [col]-->

			<aside class="col-md-3 col-sm-4">

				<!-- - - - - - - - - - - - - - Seller Information - - - - - - - - - - - - - - - - -->

				<section class="section_offset">
					<h3>Остались вопросы?</h3>
					<div class="theme_box">
						<div class="seller_info clearfix">
							<a href="#" class="alignleft photo">
								<img src="/theme/images/seller_photo_1.jpg" alt="">
							</a>
							<div class="wrapper">
								<a href="#" data-modal-url="/site/order-call"><b>Получить консультацию</b></a>
								<p class="seller_category">Обратный звонок</p>
							</div>
						</div><!--/ .seller_info-->
						
					</div><!--/ .theme_box -->
				</section>

				<!-- - - - - - - - - - - - - - End of seller information - - - - - - - - - - - - - - - - -->
		
			</aside><!--/ [col]-->
		</div><!--/ .row-->
	</div><!--/ .container-->
</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->