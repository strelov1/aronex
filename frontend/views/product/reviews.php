<?php

use yii\helpers\Html;

?>
<section class="section_offset">
	<h3>Отзывы</h3>
	<ul class="reviews">
		<li>
			<!-- - - - - - - - - - - - - - Review - - - - - - - - - - - - - - - - -->
			<article class="review">
				<!-- - - - - - - - - - - - - - Rates - - - - - - - - - - - - - - - - -->
				<ul class="review-rates">
					<!-- - - - - - - - - - - - - - Price - - - - - - - - - - - - - - - - -->
					<li class="v_centered">
						<span class="name">Price</span>
						<ul class="rating">
							<li class="active"></li>
							<li class="active"></li>
							<li class="active"></li>
							<li></li>
							<li></li>
						</ul>
					</li>

					<!-- - - - - - - - - - - - - - End of price - - - - - - - - - - - - - - - - -->

					<!-- - - - - - - - - - - - - - Value - - - - - - - - - - - - - - - - -->

					<li class="v_centered">
						<span class="name">Value</span>
						<ul class="rating">
							<li class="active"></li>
							<li class="active"></li>
							<li class="active"></li>
							<li class="active"></li>
							<li></li>
						</ul>
					</li>

					<!-- - - - - - - - - - - - - - End of value - - - - - - - - - - - - - - - - -->

					<!-- - - - - - - - - - - - - - Quality - - - - - - - - - - - - - - - - -->

					<li class="v_centered">
						<span class="name">Quality</span>
						<ul class="rating">
							<li class="active"></li>
							<li class="active"></li>
							<li class="active"></li>
							<li class="active"></li>
							<li class="active"></li>
						</ul>
					</li>

					<!-- - - - - - - - - - - - - - End of quality - - - - - - - - - - - - - - - - -->

				</ul>

				<!-- - - - - - - - - - - - - - End of rates - - - - - - - - - - - - - - - - -->

				<!-- - - - - - - - - - - - - - Review body - - - - - - - - - - - - - - - - -->

				<div class="review-body">
					<div class="review-meta">
						<h5 class="bold">Good Quality</h5>
						Review by <a href="#" class="bold">Ivana Wrong</a> on 12/4/2014
					</div>
					<p>Aliquam Erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo.</p>
				</div>

				<!-- - - - - - - - - - - - - - End of review body - - - - - - - - - - - - - - - - -->

			</article>

			<!-- - - - - - - - - - - - - - End of review - - - - - - - - - - - - - - - - -->

		</li>
		
	</ul>

	<a href="#" class="button_grey middle_btn">Показать все</a>

</section><!--/ .section_offset -->

<section class="section_offset">
	<h3>Добавтье свой отзыв</h3>
	<div class="row">
		<div class="col-lg-6">
			<p>Ваш отзыв о: <a href="#"><?= Html::encode($model->title) ?></a><br>Как вы оцениваете данный продукт? *</p>
			<!-- - - - - - - - - - - - - - Rate the - - - - - - - - - - - - - - - - -->
			<div class="table_wrap rate_table">

				<table>
					<thead>
						<tr>
							<th></th>
							<th>1 Звезда</th>
							<th>2 Звезды</th>
							<th>3 Звезды</th>
							<th>4 Звезды</th>
							<th>5 Звезды</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Цена</td>
							<td>
								<input checked type="radio" name="price_rate" id="rate_1">
								<label for="rate_1"></label>
							</td>
							<td>
								<input type="radio" name="price_rate" id="rate_2">
								<label for="rate_2"></label>
							</td>
							<td>
								<input type="radio" name="price_rate" id="rate_3">
								<label for="rate_3"></label>
							</td>
							<td>
								<input type="radio" name="price_rate" id="rate_4">
								<label for="rate_4"></label>
							</td>
							<td>	
								<input type="radio" name="price_rate" id="rate_5">
								<label for="rate_5"></label>
							</td>
						</tr>

						<tr>
							<td>Value</td>
							<td>
								<input checked type="radio" name="value_rate" id="rate_6">
								<label for="rate_6"></label>
							</td>
							<td>
								<input type="radio" name="value_rate" id="rate_7">
								<label for="rate_7"></label>
							</td>
							<td>
								<input type="radio" name="value_rate" id="rate_8">
								<label for="rate_8"></label>
							</td>
							<td>
								<input type="radio" name="value_rate" id="rate_9">
								<label for="rate_9"></label>
							</td>
							<td>
									
								<input type="radio" name="value_rate" id="rate_10">
								<label for="rate_10"></label>
							</td>
						</tr>

						<tr>
							<td>Качество</td>
							<td>
								<input checked type="radio" name="quality_rate" id="rate_11">
								<label for="rate_11"></label>
							</td>
							<td>
								<input type="radio" name="quality_rate" id="rate_12">
								<label for="rate_12"></label>
							</td>
							<td>
								<input type="radio" name="quality_rate" id="rate_13">
								<label for="rate_13"></label>
							</td>
							<td>
								<input type="radio" name="quality_rate" id="rate_14">
								<label for="rate_14"></label>
							</td>
							<td>
								<input type="radio" name="quality_rate" id="rate_15">
								<label for="rate_15"></label>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<!-- - - - - - - - - - - - - - End of rate the - - - - - - - - - - - - - - - - -->

		</div><!--/ [col]-->

		<div class="col-lg-6">
			<p class="subcaption">Все поля должны быть заполенны.</p>

			<!-- - - - - - - - - - - - - - Review form - - - - - - - - - - - - - - - - -->

			<form class="type_2">
				<ul>
					<li class="row">
						<div class="col-sm-6">
							<label for="nickname">Имя</label>
							<input type="text" name="" id="nickname">
						</div>
						<div class="col-sm-6">
							
							<label for="summary">Ваша суммарная оценка</label>
							<input type="text" name="" id="summary">
						</div>
					</li>
					<li class="row">
						<div class="col-xs-12">
							<label for="review_message">Отзыв</label>
							<textarea rows="5" id="review_message"></textarea>
						</div>
					</li>
					<li class="row">
						<div class="col-xs-12">
							<button class="button_dark_grey middle_btn">Отправить отзыв</button>
						</div>
					</li>
				</ul>
			</form>

			<!-- - - - - - - - - - - - - - End of review form - - - - - - - - - - - - - - - - -->

		</div>
	</div><!--/ .row -->
</section><!--/ .section_offset -->