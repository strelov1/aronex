<section class="section_offset">
	<h3 class="offset_title">Другие товары данной категории</h3>
	<div class="owl_carousel other_products">

		<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->
		<div class="product_item">
			<!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->
			<div class="image_wrap">
				<img src="/theme/images/product_img_6.jpg" alt="">
				<!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->
				<div class="actions_wrap">
					<div class="centered_buttons">
						<a href="#" class="button_blue middle_btn add_to_cart">Add to Cart</a>
					</div><!--/ .centered_buttons -->
				</div><!--/ .actions_wrap-->
				<!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->
			</div><!--/. image_wrap-->
			<!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->
			<!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->
			<div class="description">
				<a href="#">Enzymatic Therapy CoQ10, 100mg, Softgels 120 ea</a>
				<div class="clearfix product_info">
					<p class="product_price alignleft"><b>$75.39</b></p>
				</div>
			</div>
			<!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->
		</div><!--/ .product_item-->
		<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

	</div><!--/ .owl_carousel -->
</section><!--/ .section_offset -->