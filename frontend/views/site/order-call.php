<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
?>
<div id="order_call" class="modal_window">
    <button class="close arcticmodal-close"></button>
    <header class="on_the_sides">
        <div class="left_side">
            <h2>Заказать обратный звонок</h2>
        </div>
    </header><!--/ .on_the_sides-->


    <?php $form = ActiveForm::begin(['id' => 'order-call-form']); ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'phone') ?>
    <div class="form-group">
        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>


</div>