<?php
use yii\helpers\Html;
use yii\widgets\Menu;
use frontend\widgets\CategorListView;
use frontend\widgets\CategorMenu;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
$title = Yii::$app->name;
$this->title = Html::encode($title);
?>

<!-- - - - - - - - - - - - - - Categories - - - - - - - - - - - - - - - - -->

<aside class="col-md-3 has_mega_menu">
	<section class="section_offset">
		<h3>Категории</h3>

		<?= CategorMenu::widget([
		  'items' => $menuItems,
		  'options' => [
		      'class' => 'theme_menu cats',
		  ],
		  'parentItemOptions' => [
		      'class' => 'has_megamenu',
		  ],
		  'submenuTemplate' => "\n<div class=\"mega_menu clearfix\">
		  							<div class=\"mega_menu_item\">
		  								<ul class=\"list_of_links\">
		  									{items}
		  								</ul>
		  							</div>
		  						</div>\n",
		]) ?>
	</section>
</aside><!--/[col]-->

<!-- - - - - - - - - - - - - - End of categories - - - - - - - - - - - - - - - - -->

<!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

<?= $this->render('slider', [
    'slider' => $slider,
]) ?>

<!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

<!-- - - - - - - - - - - - - - Today's deals - - - - - - - - - - - - - - - - -->

<section class="section_offset animated transparent" data-animation="fadeInDown"> 
	<h3 class="section_title">Популярные товары</h3>
	<div class="tabs type_2 products">
		<!-- - - - - - - - - - - - - - Navigation of tabs - - - - - - - - - - - - - - - - -->
		<?= Tabs::widget([
		    'items' => $catList
		    // [
		    //     [
		    //         'label' => 'One',
		    //         'content' => 'Anim pariatur cliche...',
		    //         'active' => true
		    //     ],
		    //     [
		    //         'label' => 'Two',
		    //         'content' => 'Anim pariatur cliche...',
		    //     ],
		    // ],
		]);





			/*CatList::widget([
			  'items' => $catList,
			  'options' => [
			      'class' => 'tabs_nav clearfix',
				  'submenuTemplate' => "",
			  ],
			]); */
			?>

		<!-- - - - - - - - - - - - - - End navigation of tabs - - - - - - - - - - - - - - - - -->
		<!-- - - - - - - - - - - - - - Tabs container - - - - - - - - - - - - - - - - -->
		<div class="tab_containers_wrap">
			<div id="tab-1" class="tab_container">
				<?= CategorListView::widget([
					'dataProvider' => $todayProduct,
					'itemView' => '_product_cat',
					'separatorListDiv' => 'table_row',
					'separatorListCount' => 6,
					'layout' => "<div class=\"table_layout\" id=\"products_container\">
											\n\t{items}\n
								 </div>",
					'itemOptions' => ['class' => 'table_cell'],
				]);?>
			</div><!--/ #tab-1-->
		</div>
		<!-- - - - - - - - - - - - - - End of tabs containers - - - - - - - - - - - - - - - - -->
		<br><br><br>
		<?= $page->content; ?>
	</div>
</section><!--/ .section_offset-->

<!-- - - - - - - - - - - - - - End of taday's deals - - - - - - - - - - - - - - - - -->