<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$mainMenu = [
                ['label' => 'Главная', 'url' => ['/']],
                ['label' => 'О магазине', 'url' => ['/pages/about']],
                ['label' => 'Новости', 'url' => ['/news']],
                ['label' => 'Доставка и оплата', 'url' => ['/pages/delevery']],
                // ['label' => 'Гарантия', 'url' => ['/pages/grarant']],
                ['label' => 'Контакты', 'url' => ['/pages/contact']],
            ];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <script src="/theme/js/modernizr.js"></script>
    <?php $this->head() ?>

</head>
<body class="front_page">
<?php $this->beginBody() ?>
    <!-- - - - - - - - - - - - - Main Wrapper - - - - - - - - - - - - - - - - -->
    <div class="wide_layout">

    <!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->
<?= Alert::widget() ?>

<?= $this->render('header', [
    'mainMenu' => $mainMenu,
]) ?>

    <!-- - - - - - - - - - - - - - End Header - - - - - - - - - - - - - - - -->

    <!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

    <div class="page_wrapper">
        <div class="container">
            <div class="section_offset">
                <div class="row">

                    <?= $content ?>
            
                </div><!--/ .row-->
            </div><!--/ .section_offset-->

            <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->

            <?= $this->render('infoblocks.php')?>

            <!-- - - - - - - - - - - - - - End of infoblocks - - - - - - - - - - - - - - - - -->

        </div><!--/ .container-->
    </div><!--/ .page_wrapper-->

    <!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->

    <?= $this->render('footer', [
        'mainMenu' => $mainMenu,
    ]) ?>


<?php $this->endBody() ?>

    </body>
</html>
<?php $this->endPage() ?>
