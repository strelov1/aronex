<?php

use yii\helpers\Html;
use yii\widgets\Menu;

?>
<header id="header" class="type_2">

    <!-- - - - - - - - - - - - - - Bottom part - - - - - - - - - - - - - - - - -->
    <div class="bottom_part">
        <div class="sticky_part">
            <div class="container">
                <div class="row">
                    <div class="main_header_row">
                        <div class="col-sm-3">
                            <!-- - - - - - - - - - - - - - Logo - - - - - - - - - - - - - - - - -->
                            <a href="/" class="logo">
                                <img src="/theme/logo.png" alt="Aronex">
                            </a>
                            <!-- - - - - - - - - - - - - - End of logo - - - - - - - - - - - - - - - - -->
                        </div><!--/ [col]-->
                        <div class="col-lg-6 col-md-5 col-sm-6">
                            <!-- - - - - - - - - - - - - - Navigation of shop - - - - - - - - - - - - - - - - -->

                            <nav>
                            <?= Menu::widget([
                              'items' => $mainMenu,
                              'options' => [
                                  'class' => 'topbar',
                              ],
                            ]) ?>
                            </nav>

                            <!-- - - - - - - - - - - - - - End navigation of shop - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Search form - - - - - - - - - - - - - - - - -->

                            <form class="clearfix search" action="/catalog/search">
                                <input type="text" name="param" tabindex="1" placeholder="Поиск..." class="alignleft">
                                <button class="button_blue def_icon_btn alignleft"></button>
                            </form><!--/ #search-->

                            <!-- - - - - - - - - - - - - - End search form - - - - - - - - - - - - - - - - -->

                        </div><!--/ [col]-->
                        <style>

                        </style>
                        <div class="col-lg-3 col-md-4 col-sm-3">
                            <div class="contact">
                                <a href="tel:+78633090832" class="tel">+7 (863) 309 08 32</a><br>
                                <a href="#" data-modal-url="/site/order-call" class="oreder_call">заказать обратный звонок</a>
                            </div>
                            <div class="align_right v_centered">


                                <!-- - - - - - - - - - - - - - Shopping cart - - - - - - - - - - - - - - - - -->

                                <div class="shopping_cart_wrap">

                                    <button id="open_shopping_cart" class="open_button" data-amount="<?= Yii::$app->cart->getCount(); ?>">
                                        <b class="title">Корзина</b>
                                        <b class="total_price"><?= Yii::$app->cart->getCost(); ?> руб.</b>
                                    </button>

                                    <!-- - - - - - - - - - - - - - Products list - - - - - - - - - - - - - - - - -->

                                    <div class="shopping_cart dropdown">
                                        <div class="animated_item">
                                            <p class="title">Недавно добавленные в корзину</p>
                                        </div><!--/ .animated_item-->
                                        <?php foreach (Yii::$app->cart->getPositions() as $product):?>
                                        <?php if(!$product->id == 0): ?>
                                        <div class="animated_item">
                                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->
                                            <div class="clearfix sc_product">
                                                <?php if(!empty($product->images)): ?>
                                                    <a href="/product/view?id=<?= Html::encode($product->id) ?>" class="product_thumb">
                                                        <?= Html::img('/images/product/'.$product->images[0]->url, ['width'=> '60']);?>
                                                    </a>
                                                <?php else: ?>
                                                    <img src="/theme/images/sc_img_1.jpg" />
                                                <?php endif; ?>

                                                <a href="/product/view?id=<?=$product->id;?>" class="product_name"><?= $product->title ?></a>
                                                <p><?= $product['_quantity']; ?> x <?= $product->price ?></p>
                                                <?= Html::a('', ['cart/remove', 'id' => $product->id], ['class' => 'close'])?>
                                            </div><!--/ .clearfix.sc_product-->
                                            <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->
                                        </div><!--/ .animated_item-->
                                        <?php endif; ?>
                                        <?php endforeach; ?>

                                        <div class="animated_item">
                                            <!-- - - - - - - - - - - - - - Total info - - - - - - - - - - - - - - - - -->
                                            <ul class="total_info">
                                                <li class="total"><b><span class="price">Всего:</span> <?= Yii::$app->cart->getCost(); ?> руб.</b></li>
                                            </ul>
                                            <!-- - - - - - - - - - - - - - End of total info - - - - - - - - - - - - - - - - -->
                                        </div><!--/ .animated_item-->

                                        <div class="animated_item">
                                            <?= Html::a('В корзину', ['cart/list'], ['class' => 'button_grey'])?>
                                            <?= Html::a('Оформление', ['cart/order-step-one'], ['class' => 'button_blue'])?>
                                        </div><!--/ .animated_item-->

                                    </div><!--/ .shopping_cart.dropdown-->
                                    <!-- - - - - - - - - - - - - - End of products list - - - - - - - - - - - - - - - - -->
                                    
                                </div><!--/ .shopping_cart_wrap.align_left-->
                                
                                <!-- - - - - - - - - - - - - - End of shopping cart - - - - - - - - - - - - - - - - -->

                            </div><!--/ .align_right-->
                        </div><!--/ [col]-->
                    </div><!--/ .main_header_row-->
                </div><!--/ .row-->
            </div><!--/ .container-->
        </div>

    </div><!--/ .bottom_part -->

    <!-- - - - - - - - - - - - - - End of bottom part - - - - - - - - - - - - - - - - -->

</header>
