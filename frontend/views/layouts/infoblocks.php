<div class="infoblocks_container">
	<ul class="infoblocks_wrap">
		<li>
			<a href="#" class="infoblock type_1">
				<i class="icon-paper-plane"></i>
				<span class="caption"><b>Быстрая Доставка</b></span>
			</a><!--/ .infoblock-->
		</li>
		<li>
			<a href="#" class="infoblock type_1">
				<i class="icon-lock"></i>
				<span class="caption"><b>Гарантия качества</b></span>
			</a><!--/ .infoblock-->
		</li>
		<li>
			<a href="#" class="infoblock type_1">
				<i class="icon-money"></i>
				<span class="caption"><b>Бонусы</b></span>
			</a><!--/ .infoblock-->
		</li>
	</ul><!--/ .infoblocks_wrap.section_offset.clearfix-->
</div><!--/ .infoblocks_container -->