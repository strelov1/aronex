<?php

use yii\widgets\Menu;

?>
<!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->
<footer id="footer">
    <!-- - - - - - - - - - - - - - Footer section - - - - - - - - - - - - - - - - -->

    <div class="footer_section_3 align_center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">

                    <!-- - - - - - - - - - - - - - Payments - - - - - - - - - - - - - - - - -->

                    <ul class="payments">
                        <li><img src="/theme/images/payment_2.png" alt=""></li>
                        <li><img src="/theme/images/payment_3.png" alt=""></li>
                        <li><img src="/images/russian_post.gif" alt=""></li>
                        <li><img src="/images/ems.gif" alt=""></li>
                        <li><img src="/images/sdek.gif" alt=""></li>
                    </ul>
                    
                    <!-- - - - - - - - - - - - - - End of payments - - - - - - - - - - - - - - - - -->

                    <!-- - - - - - - - - - - - - - Footer navigation - - - - - - - - - - - - - - - - -->
                    <nav class="bottombar">
                    <?= Menu::widget([
                      'items' => $mainMenu,
                      'options' => [
                          'class' => 'bottombar',
                      ],
                    ]) ?>
                    </nav>
                    <!-- - - - - - - - - - - - - - End of footer navigation - - - - - - - - - - - - - - - - -->
                    <p class="copyright">&copy; 2015 - <?= date('Y') ?> <a href="/"><?= Yii::$app->name ?></a>. Все права защищены.</p>
                </div>
            </div>
        </div><!--/ .container-->
    </div><!--/ .footer_section-->
    <!-- - - - - - - - - - - - - - End footer section - - - - - - - - - - - - - - - - -->
</footer>
<!-- - - - - - - - - - - - - - End Footer - - - - - - - - - - - - - - - - -->
</div><!--/ [layout]-->
<!-- - - - - - - - - - - - - - End Main Wrapper - - - - - - - - - - - - - - - - -->
<!-- Yandex.Metrika counter -->
<script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter33717959 = new Ya.Metrika({ id:33717959, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/33717959" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Start SiteHeart code -->
<script>
(function(){
var widget_id = 813859;
_shcp =[{widget_id : widget_id}];
var lang =(navigator.language || navigator.systemLanguage 
|| navigator.userLanguage ||"en")
.substr(0,2).toLowerCase();
var url ="widget.siteheart.com/widget/sh/"+ widget_id +"/"+ lang +"/widget.js";
var hcc = document.createElement("script");
hcc.type ="text/javascript";
hcc.async =true;
hcc.src =("https:"== document.location.protocol ?"https":"http")
+"://"+ url;
var s = document.getElementsByTagName("script")[0];
s.parentNode.insertBefore(hcc, s.nextSibling);
})();
</script>
<!-- End SiteHeart code -->
<script type="text/javascript">
    var ZCallbackWidgetLinkId  = 'edc2cfc8343a8877b7e7f7558962b84a';
    var ZCallbackWidgetDomain  = 'ss.zadarma.com';
    (function() {
        var lt = document.createElement('script');
        lt.type ='text/javascript';
        lt.charset = 'utf-8';
        lt.async = true;
        lt.src = 'https://' + ZCallbackWidgetDomain + '/callbackWidget/js/main.min.js?unq='+Math.floor(Math.random(0,1000)*1000);
        var sc = document.getElementsByTagName('script')[0];
        if (sc) sc.parentNode.insertBefore(lt, sc);
        else document.documentElement.firstChild.appendChild(lt);
    })();
</script>
