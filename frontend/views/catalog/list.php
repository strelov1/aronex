<?php
use yii\helpers\Html;
use frontend\widgets\CategorListView;
use frontend\widgets\SelectLinkSorter;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
$title = \yii\helpers\StringHelper::truncateWords($category['title'] , 7, '...');
$this->title = Html::encode($title);
if(!empty($category['parent_id'])){
	$this->params['breadcrumbs'][] = ['label' => $categories[$category['parent_id']]['title'], 'url' => ['/catalog/list', 'id' => $categories[$category['parent_id']]['id']]];
}
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="secondary_page_wrapper">
	<div class="container">
		<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
		<?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
		<!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->
		<div class="row">
			<aside class="col-md-3 col-sm-4 has_mega_menu">

				<!-- - - - - - - - - - - - - - Categories - - - - - - - - - - - - - - - - -->

				<?= $this->render('category', [
				        'menuItems' => $menuItems,
				]) ?>

				<!-- - - - - - - - - - - - - - End of categories - - - - - - - - - - - - - - - - -->

			</aside>

			<main class="col-md-9 col-sm-8">

				<!-- - - - - - - - - - - - - - Today's deals - - - - - - - - - - - - - - - - -->

				<!-- - - - - - - - - - - - - - End of today's deals - - - - - - - - - - - - - - - - -->

				<section class="section_offset">

					<h1><?= $this->title;?></h1>

					<p><?= $category->description; ?></p>

				</section>

				<!-- - - - - - - - - - - - - - Products - - - - - - - - - - - - - - - - -->

				<div class="section_offset">

						<?= CategorListView::widget([
						  'dataProvider' => $productsDataProvider,
						  'itemView' => '_product_cat',
						  'separatorListDiv' => 'table_row',
						  'separatorListCount' => 4,
						  'layout' => "<header class=\"top_box on_the_sides\">
											<div class=\"left_side clearfix v_centered\">
												<!-- - - - - - - - - - - - - - Sort by - - - - - - - - - - - - - - - - -->
												<div class=\"v_centered\">
													<span>Сортировать по:</span>{sorter}
															<!--<select name=''>{sorter}</select>-->
													</div>
												</div>
												<!-- - - - - - - - - - - - - - End of sort by - - - - - - - - - - - - - - - - -->
											</div>
										</header>
										<div class=\"table_layout\" id=\"products_container\">
											\n\t{items}\n
										</div>
						  				<footer class=\"bottom_box on_the_sides\">
											<div class=\"left_side\">
												<p>{summary}</p>
											</div>
											<div class=\"right_side\">
													{pager}
											</div>
										</footer>",
						  'sorter' => [
						  	'class' => SelectLinkSorter::className(),
						  ],
						  'itemOptions' => ['class' => 'table_cell'],
						]);?>


				</div>
				<!-- - - - - - - - - - - - - - End of products - - - - - - - - - - - - - - - - -->
			</main>

		</div><!--/ .row -->

	</div><!--/ .container-->

	<!--</div>/ .page_wrapper-->


<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->