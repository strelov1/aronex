<?php
use yii\helpers\Html;
?>
<?php /** @var $model \common\models\Product */ ?>
<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

<!-- - - - - - - - - - - - - - Thumbnail - - - - - - - - - - - - - - - - -->
<div class="image_wrap">

    <a href="/product/<?= Html::encode($model->id) ?>">
        <?php if(!empty($model->images)): ?>
            <?= Html::img('/images/product/'.$model->images[0]->url, ['width'=> '174']);?>
        <?php else: ?>
            <img src="/theme/images/product_img_7.jpg" />
        <?php endif; ?>
    </a>

    
    <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->
    <div class="actions_wrap">
        <div class="centered_buttons">
            <?= Html::a('Добавить в корзину', ['cart/add', 'id' => $model->id], [
                    'class' => 'button_blue middle_btn add_to_cart',
                ]
            );?>
        </div><!--/ .centered_buttons -->
    </div><!--/ .actions_wrap-->
    <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->
</div><!--/. image_wrap-->
<!-- - - - - - - - - - - - - - End thumbnail - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - Product description - - - - - - - - - - - - - - - - -->
<div class="description">
    <p><a href="/product/<?= Html::encode($model->id) ?>"><?= Html::encode($model->title) ?></a></p>
    <div class="clearfix product_info">
    <p class="product_price alignleft"><b><?= $model->price ?></b></p>
</div><!--/ .clearfix.product_info-->
</div>
<!-- - - - - - - - - - - - - - End of product description - - - - - - - - - - - - - - - - -->

<!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->