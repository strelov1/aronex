<div class="col-md-9">
	<div class="animated transparent" data-animation="fadeInDown" data-animation-delay="400">
		<!-- - - - - - - - - - - - - - Revolution slider - - - - - - - - - - - - - - - - -->
		<div class="revolution_slider">
			<div class="rev_slider">
				<ul>
					<?php foreach ($slider as $item): ?>

						<!-- - - - - - - - - - - - - - Slide 1 - - - - - - - - - - - - - - - - -->

						<li data-transition="papercut" data-slotamount="7">
							<img src="/images/slider/<?= md5($item->id);?>.jpg" alt="">
							<?= $item->content;?>
							<?php /*
 							<div class="caption sfl stl layer_1" data-x="left" data-hoffset="60" data-y="90" data-easing="easeOutBack" data-speed="600" data-start="900">Best Quality</div>
							<div class="caption sfl stl layer_2" data-x="left" data-y="138" data-hoffset="60" data-easing="easeOutBack" data-speed="600" data-start="1000">Medications</div>
							<div class="caption sfl stl layer_3" data-x="left" data-y="190" data-hoffset="60" data-easing="easeOutBack" data-speed="600" data-start="1100">at Low Prices</div>
							*/ ?>
						</li>

						<!-- - - - - - - - - - - - - - End of Slide 1 - - - - - - - - - - - - - - - - -->
					<?php endforeach; ?>


				</ul>
			</div><!--/ .rev_slider-->
		</div><!--/ .revolution_slider-->
		<!-- - - - - - - - - - - - - - End of Revolution slider - - - - - - - - - - - - - - - - -->
	</div><!--/ .animated.transparent-->
</div><!--/ [col]-->