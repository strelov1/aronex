<?php
use yii\helpers\Html;
use frontend\widgets\CategorListView;
use frontend\widgets\SelectLinkSorter;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
$title = 'Поиск по сайту';
$this->title = Html::encode($title);
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="secondary_page_wrapper">
	<div class="container">
		<!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
		<?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
		<!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->
		<div class="row">
			<aside class="col-md-3 col-sm-4 has_mega_menu">

				<!-- - - - - - - - - - - - - - Categories - - - - - - - - - - - - - - - - -->

				<?= $this->render('category', [
				        'menuItems' => $menuItems,
				]) ?>


				<!-- - - - - - - - - - - - - - End of categories - - - - - - - - - - - - - - - - -->

				<?php /*
				<!-- - - - - - - - - - - - - - Filter - - - - - - - - - - - - - - - - -->

				<section class="section_offset">
					<h3>Фильтр</h3>
					<form class="type_2">
						<div class="table_layout list_view">
							<div class="table_row">

								<!-- - - - - - - - - - - - - - Category filter - - - - - - - - - - - - - - - - -->

								<div class="table_cell">
									<label>Category</label>
									<div class="custom_select">
										<select name="">
											<option value="Beauty">Beauty</option>
											<option value="Personal Care">Personal Care</option>
											<option value="Diet &amp; Fitness">Diet &amp; Fitness</option>
											<option value="Baby Needs">Baby Needs</option>
										</select>
									</div>
								</div><!--/ .table_cell -->

								<!-- - - - - - - - - - - - - - End of category filter - - - - - - - - - - - - - - - - -->

								<!-- - - - - - - - - - - - - - Manufacturer - - - - - - - - - - - - - - - - -->

								<div class="table_cell">
									<fieldset>
										<legend>Manufacturer</legend>
										<ul class="checkboxes_list">
											<li>
												<input type="checkbox" checked name="manufacturer" id="manufacturer_1">
												<label for="manufacturer_1">Manufacturer 1</label>
											</li>
											<li>
												<input type="checkbox" name="manufacturer" id="manufacturer_2">
												<label for="manufacturer_2">Manufacturer 2</label>
											</li>
											<li>
												<input type="checkbox" name="manufacturer" id="manufacturer_3">
												<label for="manufacturer_3">Manufacturer 3</label>
											</li>
										</ul>
									</fieldset>
								</div><!--/ .table_cell -->

								<!-- - - - - - - - - - - - - - End manufacturer - - - - - - - - - - - - - - - - -->

								<!-- - - - - - - - - - - - - - Price - - - - - - - - - - - - - - - - -->

								<div class="table_cell">
									<fieldset>
										<legend>Price</legend>
										<div class="range">
											Range :
											<span class="min_val"></span> - 
											<span class="max_val"></span>
											<input type="hidden" name="" class="min_value">
											<input type="hidden" name="" class="max_value">
										</div>
										<div id="slider"></div>
									</fieldset>
								</div><!--/ .table_cell -->

								<!-- - - - - - - - - - - - - - End price - - - - - - - - - - - - - - - - -->

								<!-- - - - - - - - - - - - - - Price - - - - - - - - - - - - - - - - -->

								<div class="table_cell">
									<fieldset>
										<legend>Color</legend>
										<div class="row">
											<div class="col-sm-6">
												<ul class="simple_vertical_list">
													<li>
														<input type="checkbox" name="" id="color_btn_1">
														<label for="color_btn_1" class="color_btn green">Green</label>
													</li>
													<li>
														<input type="checkbox" name="" id="color_btn_2">
														<label for="color_btn_2" class="color_btn yellow">Yellow</label>
													</li>
													<li>
														<input type="checkbox" name="" id="color_btn_3">
														<label for="color_btn_3" class="color_btn red">Red</label>
													</li>
												</ul>
											</div>
											<div class="col-sm-6">
												<ul class="simple_vertical_list">
													<li>
														<input type="checkbox" name="" id="color_btn_4">
														<label for="color_btn_4" class="color_btn blue">Blue</label>
													</li>
													<li>
														<input type="checkbox" name="" id="color_btn_5">
														<label for="color_btn_5" class="color_btn grey">Grey</label>
													</li>
													<li>
														<input type="checkbox" name="" id="color_btn_6">
														<label for="color_btn_6" class="color_btn orange">Orange</label>
													</li>
												</ul>
											</div>
										</div>
									</fieldset>
								</div><!--/ .table_cell -->

								<!-- - - - - - - - - - - - - - End price - - - - - - - - - - - - - - - - -->

							</div><!--/ .table_row -->
						</div><!--/ .table_layout -->

						<footer class="bottom_box">
							<div class="buttons_row">
								<button type="submit" class="button_blue middle_btn">Search</button>
								<button type="reset" class="button_grey middle_btn filter_reset">Reset</button>
							</div>
						</footer>
					</form>
				</section>

				<!-- - - - - - - - - - - - - - End of filter - - - - - - - - - - - - - - - - -->

				<!-- - - - - - - - - - - - - - Banner - - - - - - - - - - - - - - - - -->

				<div class="section_offset">
					<a href="#" class="banner">
						<img src="/theme/images/banner_img_10.png" alt="">
					</a>
				</div>

				<!-- - - - - - - - - - - - - - End of banner - - - - - - - - - - - - - - - - -->

				<!-- - - - - - - - - - - - - - Already viewed products - - - - - - - - - - - - - - - - -->

				<section class="section_offset">
					<h3>Already Viewed Products</h3>
					<ul class="products_list_widget">

						<!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

						<li>
							<a href="#" class="product_thumb">
								<img src="/theme/images/product_thumb_4.jpg" alt="">
							</a>
							<div class="wrapper">
								<a href="#" class="product_title">Aenean auctor wisi et urna...</a>
								<div class="clearfix product_info">
									<p class="product_price alignleft"><b>$5.99</b></p>
									<!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->
									<ul class="rating alignright">
										<li class="active"></li>
										<li class="active"></li>
										<li class="active"></li>
										<li class="active"></li>
										<li></li>
									</ul>
									<!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->
								</div>
							</div>
						</li>

						<!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

					</ul><!--/ .list_of_products-->
				</section>

				<!-- - - - - - - - - - - - - - End of already viewed products - - - - - - - - - - - - - - - - -->


				<!-- - - - - - - - - - - - - - Tags - - - - - - - - - - - - - - - - -->

				<section class="section_offset">
					<h3>Tags</h3>
					<div class="tags_container">
						<ul class="tags_cloud">
							<li><a href="#" class="button_grey">allergy</a></li>
							<li><a href="#" class="button_grey">baby</a></li>
							<li><a href="#" class="button_grey">beauty</a></li>
							<li><a href="#" class="button_grey">ear care</a></li>
							<li><a href="#" class="button_grey">for her</a></li>
							<li><a href="#" class="button_grey">for him</a></li>
							<li><a href="#" class="button_grey">first aid</a></li>
							<li><a href="#" class="button_grey">gift sets</a></li>
							<li><a href="#" class="button_grey">spa</a></li>
							<li><a href="#" class="button_grey">hair care</a></li>
							<li><a href="#" class="button_grey">herbs</a></li>
							<li><a href="#" class="button_grey">medicine</a></li>
							<li><a href="#" class="button_grey">natural</a></li>
							<li><a href="#" class="button_grey">oral care</a></li>
							<li><a href="#" class="button_grey">pain</a></li>
							<li><a href="#" class="button_grey">pedicure</a></li>
							<li><a href="#" class="button_grey">personal care</a></li>
							<li><a href="#" class="button_grey">probiotics</a></li>
						</ul><!--/ .tags_cloud-->
					</div><!--/ .tags_container-->
				</section>

				<!-- - - - - - - - - - - - - - End of tags - - - - - - - - - - - - - - - - -->

				<!-- - - - - - - - - - - - - - Banner - - - - - - - - - - - - - - - - -->

				<div class="section_offset">
					<a href="#" class="banner">
						<img src="/theme/images/banner_img_11.png" alt="">
					</a>
				</div>

				<!-- - - - - - - - - - - - - - End of banner - - - - - - - - - - - - - - - - -->
			*/ ?>
			</aside>

			<main class="col-md-9 col-sm-8">

				<!-- - - - - - - - - - - - - - Today's deals - - - - - - - - - - - - - - - - -->

				<!-- - - - - - - - - - - - - - End of today's deals - - - - - - - - - - - - - - - - -->

				<section class="section_offset">

					<h1><?= $this->title;?></h1>

					<p></p>

				</section>

				<!-- - - - - - - - - - - - - - Products - - - - - - - - - - - - - - - - -->

				<div class="section_offset">

						<?= CategorListView::widget([
						  'dataProvider' => $productsDataProvider,
						  'itemView' => '_product_cat',
						  'separatorListDiv' => 'table_row',
						  'separatorListCount' => 4,
						  'layout' => "<div class=\"table_layout\" id=\"products_container\">
											\n\t{items}\n
										</div>
						  				<footer class=\"bottom_box on_the_sides\">
											<div class=\"left_side\">
												<p>{summary}</p>
											</div>
											<div class=\"right_side\">
													{pager}
											</div>
										</footer>",
						  'sorter' => [
						  	'class' => SelectLinkSorter::className(),
						  ],
						  'itemOptions' => ['class' => 'table_cell'],
						]);?>


				</div>
				<!-- - - - - - - - - - - - - - End of products - - - - - - - - - - - - - - - - -->
			</main>

		</div><!--/ .row -->

	</div><!--/ .container-->

	<!--</div>/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->