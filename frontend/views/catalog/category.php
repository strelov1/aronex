<?php
use frontend\widgets\CategorMenu;
?>
<section class="section_offset">
	<h3>Категории</h3>
	<?= CategorMenu::widget([
	  'items' => $menuItems,
	  'options' => [
	      'class' => 'theme_menu cats',
	  ],
	  'parentItemOptions' => [
	      'class' => 'has_megamenu',
	  ],
	  'submenuTemplate' => "\n<div class=\"mega_menu clearfix\">
	  							<div class=\"mega_menu_item\">
	  								<ul class=\"list_of_links\">
	  									{items}
	  								</ul>
	  							</div>
	  						</div>\n",
	]) ?>
</section>