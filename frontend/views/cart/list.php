<?php
use \yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $products common\models\Product[] */
$this->params['breadcrumbs'][] = 'Корзина';
$quantity = '';
?>
<section class="section_offset">
    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->
    <h1>Корзина</h1>
    <!-- - - - - - - - - - - - - - Shopping cart table - - - - - - - - - - - - - - - - -->
    <div class="table_wrap">
        <table class="table_type_1 shopping_cart_table">
            <thead>
            <tr>
                <th class="product_image_col"></th>
                <th class="product_title_col">Наименование</th>
                <th>Цена</th>
                <th class="product_qty_col">Количество</th>
                <th>Итого</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($products as $product):?>
                <tr>
                    <!-- - - - - - - - - - - - - - Product Image - - - - - - - - - - - - - - - - -->
                    <td class="product_image_col" data-title="Product Image">
                        <a href="/product/view?id=<?=$product->id;?>" class="product_thumb">
                        <?php if(!empty($product->images)): ?>
                            <?= Html::img('/images/product/'.$product->images[0]->url, ['width'=> '83','height'=> '83']);?>
                        <?php else: ?>
                            <img src="/theme/images/shopping_cart_img_3.jpg" alt="">
                        <?php endif; ?>
                        </a>
                    </td>
                    <!-- - - - - - - - - - - - - - End of product Image - - - - - - - - - - - - - - - - -->
                    <!-- - - - - - - - - - - - - - Product name - - - - - - - - - - - - - - - - -->
                    <td data-title="Product Name">
                        <a href="/product/view?id=<?=$product->id;?>" class="product_title"><?= Html::encode($product->title) ?></a>
                    </td>
                    <!-- - - - - - - - - - - - - - End of product name - - - - - - - - - - - - - - - - -->
                    <!-- - - - - - - - - - - - - - Price - - - - - - - - - - - - - - - - -->
                    <td class="subtotal" data-title="Price">
                        <?= $product->price ?> руб.
                    </td>
                    <!-- - - - - - - - - - - - - - End of Price - - - - - - - - - - - - - - - - -->
                    <!-- - - - - - - - - - - - - - Quantity - - - - - - - - - - - - - - - - -->
                    <td data-title="Quantity">
                        <div class="qty min clearfix">

                            <?= Html::a('&#45', ['cart/update', 'id' => $product->getId(), 'quantity' => $quantity - 1], ['class' => 'theme_button', 'disabled' => ($quantity - 1) < 1])?>
                            <input type="text" name="" value="<?= $quantity = $product->getQuantity()?>">
                            <?= Html::a('&#43', ['cart/update', 'id' => $product->getId(), 'quantity' => $quantity + 1], ['class' => 'theme_button'])?>

                        </div><!--/ .qty.min.clearfix-->
                    </td>
                    <!-- - - - - - - - - - - - - - End of quantity - - - - - - - - - - - - - - - - -->
                    <!-- - - - - - - - - - - - - - Total - - - - - - - - - - - - - - - - -->
                    <td class="total" data-title="Total">
                        <?= $product->getCost() ?> руб.
                    </td>
                    <!-- - - - - - - - - - - - - - End of total - - - - - - - - - - - - - - - - -->
                </tr>
            <?php endforeach ?>

            </tbody>
        </table>
    </div><!--/ .table_wrap -->

    <footer class="bottom_box on_the_sides">
        <div class="right_side">
            <?= Html::a('Оформить заказ', ['cart/order-step-one'], ['class' => 'button_blue middle_btn'])?>
        </div>
    </footer><!--/ .bottom_box -->
    <!-- - - - - - - - - - - - - - End of shopping cart table - - - - - - - - - - - - - - - - -->
</section><!--/ .section_offset -->
<section class="col-sm-4" style="float:right">
    <h3>Итого</h3>
    <div class="table_wrap">
        <table class="zebra">
            <tfoot>

            <tr class="total">
                <td>К оплате</td>
                <td><?= $total ?> руб.</td>
            </tr>
            </tfoot>
        </table>
    </div>
    <footer class="bottom_box">
        <?= Html::a('Оформить заказ', ['cart/order-step-one'], ['class' => 'button_blue middle_btn'])?>

    </footer>
</section><!-- / [col] -->

