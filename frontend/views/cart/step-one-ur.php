<?php
use \yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $products common\models\Product[] */
$this->title = 'Оформление заказа Шаг 1';
$this->params['breadcrumbs'][] = $this->title;
?>

<link href="https://dadata.ru/static/css/lib/suggestions-15.10.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--[if lt IE 10]>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript" src="https://dadata.ru/static/js/lib/jquery.suggestions-15.10.min.js"></script>

<section class="section_offset">
    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->
    <h1>Оформление заказа</h1>

<div class="tabs type_2 products initialized many_tabs">
    <ul class="tabs_nav clearfix">
        <li><a href="/cart/order-step-one">Физическое лицо</a></li>
        <li class="active"><a href="/cart/order-step-one-ur">Юридическое лицо</a></li>
    </ul>
</div>
</section>

<!-- - - - - - - - - - - - - - Billing information - - - - - - - - - - - - - - - - -->

<section class="section_offset">
    <h3>Личные данные:</h3>
    <?php
    /* @var $form ActiveForm */
    $form = ActiveForm::begin([
        'id' => 'order-form',
        'class' => 'type_2',
    ]) ?>
    <div class="theme_box">
    <ul>
        <li class="row">
        <div class="col-sm-4">
            <?= $form->field($order, 'name') ?>
        </div><!--/ [col] -->
        <div class="col-sm-4">
                <?= $form->field($order, 'patronymic') ?>
        </div><!--/ [col] -->
        <div class="col-sm-4">
            <?= $form->field($order, 'surname') ?>
        </div><!--/ [col] -->
    </li><!--/ .row -->
    <li class="row">
        <div class="col-sm-6">
            <?= $form->field($order, 'phone') ?>
        </div><!--/ [col] -->
        <div class="col-sm-6">
            <?= $form->field($order, 'email') ?>
        </div><!--/ [col] -->
    </li><!--/ .row -->
    <li class="row">
        <div class="col-sm-6">
        </div><!--/ [col] -->
    </li><!--/ .row -->
    <li class="row">
        <div class="col-xs-6">
            <?= $form->field($order, 'region') ?>
        </div><!--/ [col] -->
        <div class="col-xs-6">
            <?= $form->field($order, 'city') ?>
        </div><!--/ [col] -->
    </li><!-- / .row -->
    <li class="row">
        <div class="col-sm-6">
            <?= $form->field($order, 'street') ?>
        </div><!--/ [col] -->
        <div class="col-sm-2">
            <?= $form->field($order, 'house') ?>
        </div><!--/ [col] -->
        <div class="col-sm-2">
            <?= $form->field($order, 'apartment') ?>
        </div><!--/ [col] -->
        <div class="col-sm-2">
            <?= $form->field($order, 'postal_code') ?>
        </div><!--/ [col] -->
    </li><!--/ .row -->

    <li class="row">
        <div class="col-sm-3">
            <?= $form->field($order, 'orgname') ?>
        </div><!--/ [col] -->
        <div class="col-sm-3">
            <?= $form->field($order, 'inn') ?>
        </div><!--/ [col] -->
        <div class="col-sm-3">
            <?= $form->field($order, 'ogrn') ?>
        </div><!--/ [col] -->
        <div class="col-sm-3">
            <?= $form->field($order, 'current_account') ?>
        </div><!--/ [col] -->
        <div class="col-sm-3">
            <?= $form->field($order, 'bik') ?>
        </div><!--/ [col] -->
        <div class="col-sm-3">
            <?= $form->field($order, 'kor_account') ?>
        </div><!--/ [col] -->
        <div class="col-sm-3">
            <?= $form->field($order, 'bank') ?>
        </div><!--/ [col] -->
    </li><!--/ .row -->
    <li class="row">
        <div class="col-sm-12">
            <?= $form->field($order, 'notes')->textarea() ?>
        </div><!--/ [col] -->
    </li><!--/ .row -->

    </ul>
    </div>
    <footer class="bottom_box on_the_sides">
        <div class="left_side">
            <span class="prompt">Обязательные поля</span>
        </div>
        <div class="right_side">
            <?= Html::submitButton('Следующий Шаг', ['class' => 'button_blue middle_btn']) ?>
        </div>
    </footer>

</section><!--/ .section_offset -->
<!-- - - - - - - - - - - - - - End of billing information - - - - - - - - - - - - - - - - -->

<script type="text/javascript">
$("#order-email").suggestions({
    serviceUrl: "https://dadata.ru/api/v2",
    token: "294cf289396862989dc3622f83d5fd63b02aca96",
    type: "EMAIL",
    count: 5,
    onSelect: function(suggestion) {
        console.log(suggestion);
    }
});

(function($) {
    var serviceUrl="https://dadata.ru/api/v2",
        token = "294cf289396862989dc3622f83d5fd63b02aca96",
        type  = "ADDRESS",
        $region = $("#order-region"),
        $city   = $("#order-city"),
        $street = $("#order-street"),
        $house  = $("#order-house");

    // регион и район
    $region.suggestions({
        serviceUrl: serviceUrl,
        token: token,
        type: type,
        hint: false,
        bounds: "region-area"
    });

    // город и населенный пункт
    $city.suggestions({
        serviceUrl: serviceUrl,
        token: token,
        type: type,
        hint: false,
        bounds: "city-settlement",
        constraints: $region
    });

    // улица
    $street.suggestions({
        serviceUrl: serviceUrl,
        token: token,
        type: type,
        hint: false,
        bounds: "street",
        constraints: $city
    });

    // дом
    $house.suggestions({
        serviceUrl: serviceUrl,
        token: token,
        type: type,
        hint: false,
        bounds: "house",
        constraints: $street
    });

})(jQuery);

(function($) {
    "use strict";
    function init($surname, $name, $patronymic) {
        var self = {};
        self.$surname = $surname;
        self.$name = $name;
        self.$patronymic = $patronymic;
        var fioParts = ["SURNAME", "NAME", "PATRONYMIC"];
        // инициализируем подсказки на всех трех текстовых полях
        // (фамилия, имя, отчество)
        $.each([$surname, $name, $patronymic], function(index, $el) {
            var sgt = $el.suggestions({
                serviceUrl: "https://dadata.ru/api/v2",
                token: "294cf289396862989dc3622f83d5fd63b02aca96",
                type: "NAME",
                triggerSelectOnSpace: false,
                hint: "",
                noCache: true,
                params: {
                    // каждому полю --- соответствующая подсказка
                    parts: [fioParts[index]]
                },
                onSearchStart: function(params) {
                    // если пол известен на основании других полей,
                    // используем его
                    var $el = $(this);
                    params.gender = isGenderKnown.call(self, $el) ? self.gender : "UNKNOWN";
                },
                onSelect: function(suggestion) {
                    // определяем пол по выбранной подсказке
                    self.gender = suggestion.data.gender;
                }
            });
        });
    };
    function isGenderKnown($el) {
        var self = this;
        var surname = self.$surname.val(),
            name = self.$name.val(),
            patronymic = self.$patronymic.val();
        if (($el.attr('id') == self.$surname.attr('id') && !name && !patronymic) ||
            ($el.attr('id') == self.$name.attr('id') && !surname && !patronymic) ||
            ($el.attr('id') == self.$patronymic.attr('id') && !surname && !name)) {
            return false;
        } else {
            return true;
        }
    }
    init($("#order-surname"), $("#order-name"), $("#order-patronymic"));
})(window.jQuery);
</script>
<?php ActiveForm::end() ?>