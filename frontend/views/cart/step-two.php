<?php
use \yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use common\models\Order;

$this->title = 'Оформление заказа Шаг 2';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="section_offset">
    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->
</section>

<!-- - - - - - - - - - - - - - Order review - - - - - - - - - - - - - - - - -->
<?php
$form = ActiveForm::begin([
    'id' => 'order-form',
    'class' => 'type_2',
]) ?>

<!-- - - - - - - - - - - - - - Shipping method - - - - - - - - - - - - - - - - -->

<section class="section_offset" id="delevery">
    <h3>Способ доставки</h3>
    <div class="theme_box">
        <p class="subcaption"><b>Стоимость доставки до: <?= $order->region .' '. $order->city .' '. $order->street  .' '.  $order->house .' - '. $order->apartment; ?></b></p>
        <?php $delevery = Order::getDelevery($order->city);
            if($delevery != false){
               echo $form->field($order, 'delevery')->radioList($delevery,
                   [
                       'item' => function($index, $label, $name, $checked, $value) {
                           $return  = '<label class="modal-radio">';
                           $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
                           $return .= '<span>' .  $label . '</span>';
                           $return .= '</label>';
                           return $return;
                       }
                   ]
               ) ->label(false);;
            } else {
                echo '<b class="required">Не корректно введен город</b>
                      <p>Для того чтобы расчитать стоимость доставки, необходимо ввести корректное название города доставки<br>
                      Если у вас возникли проблемы с этим, пожалуйста обратитесь в онлайн чат поддержки сайта</p>';
            }
        ?>
    </div>
     <footer class="bottom_box on_the_sides">
         <div class="left_side">
            <a href="/cart/order-step-one" class="button_blue middle_btn">Вернуться</a>
        </div>
        <div class="right_side">
            <?= Html::submitButton('Следующий Шаг', ['class' => 'button_blue middle_btn']) ?>
        </div>
    </footer>
</section>
<!-- - - - - - - - - - - - - - End of shipping method - - - - - - - - - - - - - - - - -->

<?php ActiveForm::end() ?>