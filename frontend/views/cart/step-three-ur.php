<?php
use \yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;

$this->title = 'Оформление заказа Шаг 3';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="section_offset">
    <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->
</section>

<!-- - - - - - - - - - - - - - Order review - - - - - - - - - - - - - - - - -->
<?php
$form = ActiveForm::begin([
    'id' => 'order-form',
    'class' => 'type_2',
]) ?>
<!-- - - - - - - - - - - - - - Payment information - - - - - - - - - - - - - - - - -->

<section class="section_offset" id="payment">
    <h3>Способ оплаты</h3>
    <div class="theme_box">

        <?= $form->field($order, 'payment')->radioList([
            'Выставить счет' => 'Выставить счет',
        ],['itemOptions' => ['labelOptions'=>'']])->label(''); ?>

    </div>
    <footer class="bottom_box">
         <div class="left_side">
            <a href="/cart/order-step-two" class="button_blue middle_btn">Вернуться</a>
        </div>
    </footer>
</section>

<!-- - - - - - - - - - - - - - End of payment information - - - - - - - - - - - - - - - - -->

<!-- - - - - - - - - - - - - - Order review - - - - - - - - - - - - - - - - -->

<section class="section_offset" id="order">
    <h3>Заказ</h3>
    <div class="table_wrap">
        <table class="table_type_1 order_review">
            <thead>
            <tr>
                <th class="product_title_col">Наименование</th>
                <th class="product_price_col">Стоимость</th>
                <th class="product_qty_col">Количество</th>
                <th class="product_total_col">Итого</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($products as $product):?>
                <tr>
                    <td data-title="Product Name">
                        <a href="#" class="product_title"><?= Html::encode($product->title) ?></a>
                    </td>
                    <td data-title="Price" class="subtotal"><?= $product->price ?> руб.</td>
                    <td data-title="Quantity"><?= $quantity = $product->getQuantity()?></td>
                    <td data-title="Total" class="total"><?= $product->getCost() ?> руб.</td>
                </tr>
            <?php endforeach ?>

            </tbody>
            <tfoot>
            <tr>
                <td colspan="3" class="bold">Доставка <?= $order->delevery ?></td>
                <td class="total"><?= $order->delevery_cost ?> руб.</td>
            </tr>
            <tr>
                <td colspan="3" class="grandtotal">Итого</td>
                <td class="grandtotal"><?= $order->delevery_cost + $total ?> руб.</td>
            </tr>
            </tfoot>
        </table>
    </div><!--/ .table_wrap -->
    <footer class="bottom_box on_the_sides">
        <div class="left_side v_centered">
            <span>Изменить количество?</span>
            <?= Html::a('Вернуться в корзину', ['cart/list'], ['class' => 'button_grey middle_btn'])?>
        </div>
        <div class="right_side">
            <?= Html::submitButton('Оформить заказ', ['class' => 'button_blue middle_btn']) ?>
        </div>
    </footer>
</section>
<!-- - - - - - - - - - - - - - End of order review - - - - - - - - - - - - - - - - -->
<?php ActiveForm::end() ?>