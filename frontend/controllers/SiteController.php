<?php
namespace frontend\controllers;

use Yii;
use frontend\models\ContactForm;
use common\models\OrderCall;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionOrderCall()
    {
        $this->layout = 'blank';
        $model = new OrderCall();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Спасибо за заявку, скором мы вам перевзоним.');
            } else {
                Yii::$app->session->setFlash('error', 'Что-то пошло не так, обратитесь к администратору сайта.');
            }

            return $this->goBack();
        } else {
            return $this->render('order-call', [
                'model' => $model,
            ]);
        }
    }

}
