<?php

namespace frontend\controllers;

use Yii;
use common\models\Category;
use common\models\Product;
use frontend\models\ProductSearch;
use common\models\Banner;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use bupy7\pages\models\Page;

class CatalogController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Url::remember();
            return true;
        } else {
            return false;
        }
    }
 
    public function actionIndex()
    {
        $catall = Category::find()->indexBy('id')->orderBy('id');
        $catList = $catall->where(['parent_id' => NULL])->all();
        $categories = $catall->all();
        $slider = Banner::find()->indexBy('id')->orderBy('id')->all();
        $page =  Page::find()->where(['alias' => 'index'])->one();

        $productsQuery = Product::find()->where(['label_id' => 2]);
        $productsDataProvider = new ActiveDataProvider([
            'query' => $productsQuery,
            'pagination' => [
                'pageSize' => 18,
            ],
        ]);
        
        return $this->render('index', [
            'menuItems' => $this->getMenuItems($categories),
            'catList' => $this->getTabList($catList),
            'todayProduct' => $productsDataProvider,
            'slider' => $slider,
            'page' => $page,
        ]);
    }

    public function actionList($id)
    {
        $categories = Category::find()->indexBy('id')->orderBy('id')->all();

        $productsQuery = Product::find();
        if ($id !== null && isset($categories[$id])) {
            $category = $categories[$id];
            $productsQuery->where(['category_id' => $this->getCategoryIds($categories, $id)]);
        }

        $productsDataProvider = new ActiveDataProvider([
            'query' => $productsQuery,
            'pagination' => [
                'pageSize' => 16,
            ],
            'sort' => [
                'attributes' => [
                    'price',
                ],
            ],
        ]);

        return $this->render('list', [
            'category' => $categories[$id],
            'categories' => $categories,
            'menuItems' => $this->getMenuItems($categories, isset($category->id) ? $category->id : null),
            'productsDataProvider' => $productsDataProvider,
        ]);
    }

    public function actionSearch($param)
    {
        $categories = Category::find()->indexBy('id')->orderBy('id')->all();
        $searchModel = new ProductSearch();
        $queryParams = array_merge([],Yii::$app->request->getQueryParams());
        $queryParams['ProductSearch']['title'] = $param;
        $dataProvider = $searchModel->search($queryParams);
        // $dataProvider->sort->defaultOrder = ['group_id' => SORT_ASC];

        
        return $this->render('search', [
            'categories' => $categories,
            'menuItems' =>  $this->getMenuItems($categories),
            'productsDataProvider' => $dataProvider,
        ]);
    }

    public function actionView()
    {
        return $this->render('view');
    }

    /**
     * @param Category[] $categories
     * @param int $activeId
     * @param int $parent
     * @return array
     */
    private function getMenuItems($categories, $activeId = null, $parent = null)
    {
        $menuItems = [];
        foreach ($categories as $category) {
            if ($category->parent_id === $parent) {
                $menuItems[$category->id] = [
                    'active' => $activeId === $category->id,
                    'label' => $category->title,
                    'url' => ['catalog/list', 'id' => $category->id],
                    'items' => $this->getMenuItems($categories, $activeId, $category->id),
                ];
            }
        }
        return $menuItems;
    }


    private function getTabList($categories)
    {
        $menuItems = [];
        foreach ($categories as $category) {
                $menuItems[] = [
                    'label' => $category->title,
                    'content' => $category->description,
                ];
        }
        return $menuItems;
    }


    /**
     * Returns IDs of category and all its sub-categories
     *
     * @param Category[] $categories all categories
     * @param int $categoryId id of category to start search with
     * @param array $categoryIds
     * @return array $categoryIds
     */
    private function getCategoryIds($categories, $categoryId, &$categoryIds = [])
    {
        foreach ($categories as $category) {
            if ($category->id == $categoryId) {
                $categoryIds[] = $category->id;
            }
            elseif ($category->parent_id == $categoryId){
                $this->getCategoryIds($categories, $category->id, $categoryIds);
            }
        }
        return $categoryIds;
    }
}
