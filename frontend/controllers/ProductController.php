<?php

namespace frontend\controllers;

use Yii;
use common\models\Category;
use common\models\Product;
use common\models\Attribute;
use backend\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yz\shoppingcart\ShoppingCart;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);
        $parent_categor = Category::find()->where(['id' =>$model->category->parent_id])->indexBy('id')->orderBy('id')->one();
        $atributes = Attribute::find()->where(['product_id' => $id])->indexBy('id')->orderBy('id')->all();
        return $this->render('view', [
            'model' => $model,
            'parent_categor' => $parent_categor,
            'atributes' => $atributes,
        ]);
    }


    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
