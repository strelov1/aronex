<?php

namespace frontend\controllers;

use Yii;
use common\models\Order;
use common\models\OrderItem;
use common\models\Product;
use yz\shoppingcart\ShoppingCart;
use common\models\Edost;
use common\models\Payment;

class CartController extends \yii\web\Controller
{
    public function actionAdd($id)
    {
        $product = Product::findOne($id);
        if ($product) {
            \Yii::$app->cart->put($product);
             return $this->goBack();
        }
    }

    public function actionList()
    {
        $cart = \Yii::$app->cart;
        $products = $cart->getPositions();
        $total = $cart->getCost();

        return $this->render('list', [
           'products' => $products,
           'total' => $total,
        ]);
    }

    public function actionRemove($id)
    {
        $product = Product::findOne($id);
        if ($product) {
            \Yii::$app->cart->remove($product);
            return $this->goBack();
        }
    }

    public function actionUpdate($id, $quantity)
    {
        $product = Product::findOne($id);
        if ($product) {
            \Yii::$app->cart->update($product, $quantity);
            $this->redirect(['/cart/list']);
        }
    }

    public function actionOrderStepOne(){
        if(Yii::$app->session->get('order')){
            $id = Yii::$app->session->get('order');
            $order = Order::findOne($id);
        } else {
            $order = new Order();
        }

        $cart = Yii::$app->cart;
        $products = $cart->getPositions();
        $total = $cart->getCost();

        if ($order->load(Yii::$app->request->post()) && $order->save()) {
            return $this->redirect(['cart/order-step-two']);
        } else {
            return $this->render('step-one', [
                'order' => $order,
            ]);
        }
    }

    public function actionOrderStepOneUr(){
        if(Yii::$app->session->get('order')){
            $id = Yii::$app->session->get('order');
            $order = Order::findOne($id);
        } else {
            $order = new Order();
        }

        $order->scenario = 'one-ur';

        $cart = Yii::$app->cart;
        $products = $cart->getPositions();
        $total = $cart->getCost();

        if ($order->load(Yii::$app->request->post()) && $order->save()) {
            return $this->redirect(['cart/order-step-two-ur']);
        } else {
            return $this->render('step-one-ur', [
                'order' => $order,
            ]);
        }
    }

    public function actionOrderStepTwo(){
        $id = Yii::$app->session->get('order');
        if(!isset($id)){
        	$this->redirect(['cart/order-step-one']);
        }
        $order = Order::findOne($id);
        $order->scenario = 'delevery';

        $cart = Yii::$app->cart;
        $products = $cart->getPositions();
        $total = $cart->getCost();

        if ($order->load(Yii::$app->request->post())){
            $delevery = Order::getDeleveryOne($order->delevery, $order->city);
            $order->delevery_cost = $delevery['price'];
            $order->delevery = $delevery['tarif'];
            $order->save(false);
            return $this->redirect(['cart/order-step-three']);
        } else {
            return $this->render('step-two', [
                'order' => $order,
            ]);
        }
    }

    public function actionOrderStepTwoUr(){
        $id = Yii::$app->session->get('order');
        if(!isset($id)){
        	$this->redirect(['cart/order-step-one']);
        }
        $order = Order::findOne($id);
        $order->scenario = 'delevery';

        $cart = Yii::$app->cart;
        $products = $cart->getPositions();
        $total = $cart->getCost();

        if ($order->load(Yii::$app->request->post())){
            $delevery = Order::getDeleveryOne($order->delevery, $order->city);
            $order->delevery_cost = $delevery['price'];
            $order->delevery = $delevery['tarif'];
            $order->save(false);
            return $this->redirect(['cart/order-step-three-ur']);
        } else {
            return $this->render('step-two', [
                'order' => $order,
            ]);
        }
    }

    public function actionOrderStepThree(){
        $id = Yii::$app->session->get('order');
        if(!isset($id)){
        	$this->redirect(['cart/order-step-one']);
        }
        $order = Order::findOne($id);
        $order->scenario = 'payment';

        $cart = Yii::$app->cart;
        $products = $cart->getPositions();
        $total = $cart->getCost();

        if ($order->load(\Yii::$app->request->post()) && $order->validate()) {
            $transaction = $order->getDb()->beginTransaction();
            $order->save(false);

            foreach($products as $product) {
                $orderItem = new OrderItem();
                $orderItem->order_id = $order->id;
                $orderItem->title = $product->title;
                $orderItem->price = $product->getPrice();
                $orderItem->product_id = $product->id;
                $orderItem->quantity = $product->getQuantity();
                if (!$orderItem->save(false)) {
                    $transaction->rollBack();
                    \Yii::$app->session->addFlash('error', 'Что то пошло не так... Попробуйти связаться с администратором сайта.');
                    return $this->redirect('/cart/list');
                }
            }

            $transaction->commit();
            \Yii::$app->cart->removeAll();
            \Yii::$app->session->remove('order');
            \Yii::$app->session->addFlash('success', 'Спасибо за заказ, мы скоро свяжемяся с вами чтобы обсудить детали заказа.');
            $order->sendEmail();
            return $this->redirect(Payment::pay($order->payment, ($order->delevery_cost + $total) ));
        }

        return $this->render('step-three', [
            'order' => $order,
            'products' => $products,
            'total' => $total,
        ]);
    }

    public function actionOrderStepThreeUr(){
        $id = Yii::$app->session->get('order');
        if(!isset($id)){
        	$this->redirect(['cart/order-step-one']);
        }
        $order = Order::findOne($id);
        $order->scenario = 'payment';

        $cart = Yii::$app->cart;
        $products = $cart->getPositions();
        $total = $cart->getCost();

        if ($order->load(\Yii::$app->request->post()) && $order->validate()) {
            $transaction = $order->getDb()->beginTransaction();
            $order->save(false);

            foreach($products as $product) {
                $orderItem = new OrderItem();
                $orderItem->order_id = $order->id;
                $orderItem->title = $product->title;
                $orderItem->price = $product->getPrice();
                $orderItem->product_id = $product->id;
                $orderItem->quantity = $product->getQuantity();
                if (!$orderItem->save(false)) {
                    $transaction->rollBack();
                    \Yii::$app->session->addFlash('error', 'Что то пошло не так... Попробуйти связаться с администратором сайта.');
                    return $this->redirect('/cart/list');
                }
            }

            $transaction->commit();
            \Yii::$app->cart->removeAll();
            \Yii::$app->session->remove('order');
            \Yii::$app->session->addFlash('success', 'Спасибо за заказ, мы скоро свяжемяся с вами чтобы обсудить детали заказа.');
            $order->sendEmail();
            return $this->redirect(Payment::pay($order->payment, ($order->delevery_cost + $total) ));
        }

        return $this->render('step-three-ur', [
            'order' => $order,
            'products' => $products,
            'total' => $total,
        ]);
    }


    public function actionOrderAccount(){
        $this->layout = 'blank';
        return $this->render('step-account', [
            // 'order' => $order,
        ]);
    }

    public function actionPaymentSuccess($id){
        $this->layout = 'blank';
        $order = Order::findOne($id);
        $order->status = Order::STATUS_PAYMENT;
        $order->save(false);
        var_dump($order->status);
    }
}
