<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'theme/css/animate.css',
        'theme/css/fontello.css',
        'theme/css/style.css',
        'theme/js/owlcarousel/owl.carousel.css',
        'theme/js/rs-plugin/css/settings.css',
        'theme/js/arcticmodal/jquery.arcticmodal.css',
        'theme/js/fancybox/source/jquery.fancybox.css',
		'theme/js/fancybox/source/helpers/jquery.fancybox-thumbs.css',
		// 'theme/js/colorpicker/colorpicker.css',
    ];

    public $js = [
        'theme/js/jquery.elevateZoom-3.0.8.min.js',
        'theme/js/fancybox/source/jquery.fancybox.pack.js',
        'theme/js/fancybox/source/helpers/jquery.fancybox-media.js',
        'theme/js/fancybox/source/helpers/jquery.fancybox-thumbs.js',

        'theme/js/jquery.appear.js',
        'theme/js/owlcarousel/owl.carousel.min.js',
        'theme/js/arcticmodal/jquery.arcticmodal.js',

        // 'theme/js/colorpicker/colorpicker.js',

        'theme/js/rs-plugin/js/jquery.themepunch.tools.min.js',
        'theme/js/rs-plugin/js/jquery.themepunch.revolution.min.js',

        'theme/js/retina.min.js',
        'theme/js/theme.plugins.js',
        'theme/js/theme.core.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}